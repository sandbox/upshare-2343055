<?php

/**
 * @file
 * Administration forms for the Viral Sharing Up module.
 */ 
 
/**
 * Menu callback; Displays the administration settings for Viral Sharing Up.
 */
function viral_sharing_buttons_by_up_admin_settings() {
  drupal_add_css(drupal_get_path('module', 'viral_sharing_buttons_by_up') . '/css/up.css');
  $form = array();
  $form['upshare_partner_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Partner ID'),
    '#default_value' => variable_get('upshare_partner_id', ''),
	'#required' => TRUE,
  );
  $form['#theme'] = array('upshare_admin_settings');
  $form['#submit'][] = 'viral_sharing_buttons_by_up_admin_settings_submit';
  return system_settings_form($form);
}

/**
 * Form callback;
 */
function viral_sharing_buttons_by_up_admin_settings_submit($form, &$form_state) {
  unset($_SESSION['messages']['status']);
}