-- SUMMARY --

Get more traffic on your site and increase viral interaction by installing the UP Sharing Buttons widget! Unlike your normal, everyday share buttons, UP’s proprietary technology has been designed to help make your site more viral. 

With UP, each time users share your content on their favorite social networks, they get points they can cash in for cool stuff in the UP Store. Users sign up for UP once, then each time they share your content across networks like Facebook, Twitter, Google+, or LinkedIn, they get points – and you get powerful cross-network viral data on how your users are sharing your content on their social networks.

Our points-based sharing incentives give users a clear reason to share your content - helping raise your site’s viral coefficient. UP’s customizable platform and social share icons help you optimize your content, blog posts and pages for more social sharing engagement, and a wider viral reach.

What’s more - by signing up as an [UP Partner](https://www.upshare.co/partners/sign_up?origin=wp) for free, you get access to demographic data and sharing behavior to help you get to know your most viral users, and turn your visitors into evangelists.

You don’t just want people to share your content – you want them to start conversations around your content that their networks engage in. You also want your site to be PROMOTED to other users who are sharing similar sites on their social networks and social media accounts. 

For a full description of the module, visit the project page:
  http://drupal.org/project/admin_menu

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/admin_menu


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

* You likely want to disable Toolbar module, since its output clashes with
  Administration menu.


-- CONFIGURATION --

* Open the Plugin Settings page

* Enter your Partner ID (get it for free in 30 seconds by <a href="https://www.upshare.co/partners/sign_up?origin=dr" target="_Blank">clicking here</a>).

* The sharring buttons are now live on your site – and enjoy!
