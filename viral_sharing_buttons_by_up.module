<?php

/**
 * @file
 * The UP Sharing button Drupal module.
 */

/**
 * Implements hook_help().
 */
function viral_sharing_buttons_by_up_help($path, $arg) {
  switch ($path) {
  case 'admin/help#viral_sharing_buttons_by_up':
    $output = '<p>'. t('The Viral Sharing Buttons from <a href="@up">UP</a> change the way sites go viral. Users share content from your site and earn points they can cash in for cool stuff.', array('@up' => 'https://www.upshare.co')) .'</p>';
    $output.= '<h3>'. t('Installation') .'</h3>';
    $output.= '<ol><li>'. t('Register your site information at <a href=""https://www.upshare.co/partners/sign_up?origin=dr">UP</a>') .'</li>';
    $output.= '<li>'. t('In the <a href="@configuration">UP configuration</a>, set the partner id', array('@configuration' => url('admin/config/services/viral_sharing_buttons_by_up'))) .'</li>';
    return $output;
  case 'admin/config/services/viral_sharing_buttons_by_up':
    return '<p>'. t('The following provides the general configuration options for the <a href="@up">UP</a>.', array('@up' => 'http://www.upshare.co')) .'</p>';
  }
}


/**
 * Implements hook_menu().
 */
function viral_sharing_buttons_by_up_menu() {
  $items = array();
  $items['admin/config/services/viral_sharing_buttons_by_up'] = array(
    'title' => 'UP Settings',
    'description' => 'Provides configuration options for the UP Sharing system.',
    'access arguments' => array('administer Up'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('viral_sharing_buttons_by_up_admin_settings'),
    'file' => 'viral_sharing_buttons_by_up.admin.inc',
  );
  $items['remove_notice_message'] = array(
    'page callback' => 'remove_notice_message',
    'access arguments' => array('administer Up'),
    'type' => MENU_CALLBACK,
 );  
  return $items;
}

/**
 * Implements hook_permission().
 */
function viral_sharing_buttons_by_up_permission(){
  return array(
    'administer Up' => array(
      'title' => t('Administer Up configuration'),
    ),
  );
}

/**
 * Implements hook_theme().
 */
function viral_sharing_buttons_by_up_theme($existing, $type, $theme, $path){
  return array(
    'upshare_admin_settings' => array(
      'render element' => 'form',
      'template' => 'upshare-admin-settings',
      'path' => $path . '/templates',
    ),
  );
}

/**
 * Implements hook_init().
 */

function viral_sharing_buttons_by_up_init() {
  $current_path = current_path();
  $partner_id = variable_get('upshare_partner_id', '');
  if (isset($partner_id) && $partner_id != '') {
    if (!path_is_admin($current_path)) {
      $inline_script = '<script src="https://widget.upshare.co/v1/app/public/js/up-load.js?partnerid='.$partner_id.'" id="UPWidget"></script>';
      $element = array(
        '#type' => 'markup',
        '#markup' => $inline_script,
      );
      drupal_add_html_head($element, 'up-script');	
	}  
  }
  else {
    if (!path_is_admin($current_path)) {
      $inline_script = '<script src="https://widget.upshare.co/v1/app/public/js/up-load.js" id="UPWidget"></script>';
      $element = array(
        '#type' => 'markup',
        '#markup' => $inline_script,
      );
      drupal_add_html_head($element, 'up-script');	
	}  
	
    if (path_is_admin($current_path) && variable_get('upshare_notice', '1')) {
	  drupal_add_library('system', 'drupal.ajax');
      $message = t("Sign up for UP and every time users share content from your site they will get points they can redeem for cool stuff.") . '<br>';
      $conf_link = l(t('Enter Partner Id'), 'admin/config/services/viral_sharing_buttons_by_up');
      $signup_link = '<a href="https://www.upshare.co/partners/sign_up?origin=dr" target="_blank"> Sign Up </a>';
	  $remove_link = l(t('Ignore this notice'), 'remove_notice_message/nojs/', array('attributes' => array('class' => array('use-ajax'))));
      $message .= $conf_link .' | '. $signup_link . ' | ' . $remove_link;
      drupal_set_message($message, 'status', $repeat = FALSE);
	}  
  }
}

/**
 * Callback for notice remove link
 *
 * @param string $type
 *   Either 'ajax' or 'nojs. Type is simply the normal URL argument to this URL.
 *
 * @return string|array
 *   If $type == 'ajax', returns an array of AJAX Commands.
 *   Otherwise, just returns the content, which will end up being a page.
 *
 * @ingroup ajax_example
 */
function remove_notice_message($type = 'ajax') {
  if ($type == 'ajax') {
    $commands = array();
	variable_set('upshare_notice', '0');
	$commands[] = ajax_command_invoke('.messages', 'hide');
    $page = array('#type' => 'ajax', '#commands' => $commands);
    ajax_deliver($page);
  }
  else {
    $output = t("Javascript is not enabled.");
    return $output;
  }
}

/**
 * Implements hook_form_alter().
 */

function viral_sharing_buttons_by_up_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'system_modules') {
    $form['#submit'][] = 'upshare_refresh_message';
  }
}

function upshare_refresh_message(&$form, &$form_state) {
  unset($_SESSION['messages']['status']);
}