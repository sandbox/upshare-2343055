<?php 
  $partner_id = $form['upshare_partner_id']['#value'];
  $form['actions']['submit']['#value'] = t('Sync UP Account');
  $path = drupal_get_path('module', 'viral_sharing_buttons_by_up');
  if( $partner_id!="") { 
?>

<div class="wrap">
  <section id="header-up">
    <div class="wrapper">
      <div class="logo-up"><img src="<?php print base_path(). $path; ?>/images/logo.png">for Drupal</div>
      <div class="text-right pull-right" style="display:none;"> Need an account?<a href="https://www.upshare.co/partners/sign_up?origin=dr" target="_blank"><b> Sign Up</b></a> </div>
      <div class="border-image"><img src="<?php print base_path(). $path; ?>/images/border-img.png"></div>
    </div>
  </section>
  <section id="got-widget">
    <div class="wrapper">
      <div class="widget-main-contant">
        <h1>You've got the widget - now go viral with<span> UP </span></h1>
        <p style="padding-left:40px;padding-right:40px;">Every time users share content from your site, they'll get points they can redeem for cool stuff and you'll get viral data you can't get anywhere else.</p>
      </div>
    </div>
  </section>
  <section id="partner-id">
    <div class="wrapper">
      <div class="partner-id-main-contant1">
        <h1>UP is currently configured on your site</h1>
        <div align="center" style="padding-top: 10px;"><a class="partner-botton" href="http://www.upshare.co/partners/sign_in?" target="_blank">My Dashboard</a></div>
      </div>
    </div>
  </section>
  <section id="main-widgets">
    <div class="wrapper">
      <div class="main-widgets-box">
        <div class="widget-first"> <img src="<?php print base_path(). $path; ?>/images/yes-icon.png">
          <h1>Install the Widget</h1>
          <p>UP;s cloud-based responsive sharing widget combines lightening-fast load time with one-of-a-kind value for your users</p>
        </div>
        <div class="widget-first"> <img src="<?php print base_path(). $path; ?>/images/yes-icon.png">
          <h1>Sync your Free Account</h1>
          <p>Sign up as an UP partner for free, then enter your partner ID above to give your users points for sharing your content, and get access to the UP Partner Dashboard</p>
        </div>
        <div class="widget-second1">
          <h1>Go Viral</h1>
          <p>The formula for going viral is simple: produce good content, and give users a good reason to share it. You take care of the content. UP takes care of the rest.</p>
        </div>
      </div>
      <div align="center">
        <iframe src="//player.vimeo.com/video/105724953?byline=0" width="600" height="338" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
      </div>
    </div>
  </section>
  <section id="footer-up" class="upshare_footer">
    <div class="wrapper">
      <div class="row">
        <div class="footer-text">
          <p><a href="https://www.upshare.co/partners/sign_up?origin=dr" target="_blank">Learn More About <b>UP</b></a></p>
        </div>
      </div>
    </div>
  </section>
</div>
<?php } else { ?>
<div class="wrap">
  <section id="header-up">
    <div class="wrapper">
      <div class="logo-up"><img src="<?php print base_path(). $path; ?>/images/logo.png">for Drupal</div>
      <div class="text-right pull-right"> Need an account?<a href="https://www.upshare.co/partners/sign_up?origin=dr" target="_blank"><b> Sign Up</b></a> </div>
      <div class="border-image"><img src="<?php print base_path(). $path; ?>/images/border-img.png"></div>
    </div>
  </section>
  <section id="got-widget">
    <div class="wrapper">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="widget-main-contant">
            <h1>You've got the widget - now go viral with<span> UP </span></h1>
            <p><a href="https://www.upshare.co/partners/sign_up?origin=dr" target="_blank">Create a free account</a> and then enter your partner ID below.  Now every time users share content from your site, they'll get points they can redeem for cool stuff and you'll get viral data you can't get anywhere else.</p>
          </div>
          <div align="center">
            <iframe src="//player.vimeo.com/video/105724953?byline=0" width="600" height="338" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section id="partner-id">
    <div class="wrapper">
      <div class="partner-id-main-contant">
        <h1>Paste your partner id here:</h1>
        <?php print drupal_render_children($form) ?> 
      </div>
    </div>
  </section>
  <section id="partner-id">
    <!--<div class="wrapper">
             <div class="partner-bottom"><p>Where can I get my Partner ID?</p></div>
     </div> -->
  </section>
  <section id="main-widgets">
    <div class="wrapper">
      <div class="main-widgets-box">
        <div class="widget-first"> <img src="<?php print base_path(). $path; ?>/images/yes-icon.png">
          <h1>Install the Widget</h1>
          <p>UP;s cloud-based responsive sharing widget combines lightening-fast load time with one-of-a-kind value for your users</p>
        </div>
        <div class="widget-second">
          <h1>Sync your Free Account</h1>
          <p>Sign up as an UP partner for free, then enter your partner ID above to give your users points for sharing your content, and get access to the UP Partner Dashboard</p>
        </div>
        <div class="widget-third">
          <h1>Go Viral</h1>
          <p>The formula for going viral is simple: produce good content, and give users a good reason to share it. You take care of the content. UP takes care of the rest.</p>
        </div>
      </div>
    </div>
  </section>
  <section id="footer-up" class="upshare_footer">
    <div class="wrapper">
      <div class="row">
        <div class="footer-text">
          <p><a href="https://www.upshare.co/partners/sign_up?origin=dr" target="_blank">Learn More About <b>UP</b></a></p>
        </div>
      </div>
    </div>
  </section>
</div>
<?php } ?>